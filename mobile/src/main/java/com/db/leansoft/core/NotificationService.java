package com.db.leansoft.core;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;


import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;

import com.db.leansoft.R;

import com.db.leansoft.production.ProductionActivity;
import com.db.leansoft.repair.RepairActivity;


public class NotificationService extends  Service {

    private static final int mId = 101;

    @Override
    public void onStart(Intent intent, int startId) {

        super.onStart(intent, startId);

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.massag_icon)
                .setContentTitle("Аварийность")
                .setContentText("Аварийный останов блока № 7 ЛаТЭС");

        Intent resultIntent = new Intent(this, ProductionActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(ProductionActivity.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(mId, mBuilder.build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}



//    public void runThread() {
//            Thread myThread = new Thread(){
//                @Override
//                public void run() {
//                    try {Context context = getApplication();
//                        sleep(3000);
//                        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
//                        PendingIntent contentIntent = PendingIntent.getActivity(context,
//                                0, intent,
//                                PendingIntent.FLAG_CANCEL_CURRENT);
//
//                        Resources res = context.getResources();
//                        Notification.Builder builder = new Notification.Builder(context);
//
//                        builder.setContentIntent(contentIntent)
//                                .setSmallIcon(R.mipmap.potter)
//                                // большая картинка
//                                .setLargeIcon(BitmapFactory.decodeResource(res, R.mipmap.dtek_logo))
//                                .setTicker(res.getString(R.string.warning)) // текст в строке состояния
//                                .setTicker("Leansoft")
//                                .setWhen(System.currentTimeMillis())
//                                .setAutoCancel(true)
//                                .setContentTitle(res.getString(R.string.notifytitle)) // Заголовок уведомления
//                                .setContentTitle("Напоминание")
//                                //.setContentText(res.getString(R.string.notifytext))
//                                .setContentText("Пора покормить Влада"); // Текст уведомления
//
//                        // Notification notification = builder.getNotification(); // до API 16
//                        Notification notification = builder.build();
//
//                        NotificationManager notificationManager = (NotificationManager) context
//                                .getSystemService(Context.NOTIFICATION_SERVICE);
//                        notificationManager.notify(NOTIFY_ID, notification);
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//                };
//                    myThread.start();
//    }


