package com.db.leansoft.googleMap;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    String a = "  1";
    String b = "  2";
    String c = "  3";
    String d = "  4";
    String e = "  5";

    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng lviv = new LatLng(49.7, 24.0);
        LatLng vinnytsia = new LatLng(49.2, 28.47);
        LatLng kryviyRigh = new LatLng(47.8, 33.3);
        LatLng kiev = new LatLng(50.4, 30.5);
        LatLng kharkiv = new LatLng(49.9, 36.2);

        LatLng positions = new LatLng(49.2, 28.47);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(positions, 7));

        Marker largewebsite1 = googleMap.addMarker(new MarkerOptions().position(lviv).title("Kiel").snippet("Kiel is cool").icon(BitmapDescriptorFactory.fromResource(R.mipmap.largewebsite)));
        Marker largewebsite2 = googleMap.addMarker(new MarkerOptions().position(kiev).title("Kiel").snippet("Kiel is cool").icon(BitmapDescriptorFactory.fromResource(R.mipmap.largewebsite)));
        Marker largewebsite3 = googleMap.addMarker(new MarkerOptions().position(kryviyRigh).title("Kiel").snippet("Kiel is cool").icon(BitmapDescriptorFactory.fromResource(R.mipmap.largewebsite)));
        Marker largewebsite4 = googleMap.addMarker(new MarkerOptions().position(vinnytsia).title("Kiel").snippet("Kiel is cool").icon(BitmapDescriptorFactory.fromResource(R.mipmap.largewebsite)));
        Marker largewebsite5 = googleMap.addMarker(new MarkerOptions().position(kharkiv).title("Kiel").snippet("Kiel is cool").icon(BitmapDescriptorFactory.fromResource(R.mipmap.largewebsite)));



//        map.moveCamera(CameraUpdateFactory.newLatLng(lviv));
//        map.moveCamera(CameraUpdateFactory.newLatLng(kryviyRigh));
//        map.moveCamera(CameraUpdateFactory.newLatLng(kharkiv));
//        map.moveCamera(CameraUpdateFactory.newLatLng(vinnytsia));
//        map.moveCamera(CameraUpdateFactory.newLatLng(kiev));


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
