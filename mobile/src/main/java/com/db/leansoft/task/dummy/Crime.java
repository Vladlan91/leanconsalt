package com.db.leansoft.task.dummy;

import java.util.Date;
import java.util.UUID;

/**
 * Created by User on 13.02.2017.
 */

public class Crime {
    private UUID mId;
    private String mTitle;
    private Date mDate;
    private boolean mSolved;

    public String getTitle() {
        return mTitle;
    }

    public UUID getId() {
        return mId;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public Crime() {
        mId = UUID.randomUUID();
        mDate = new Date();
    }
    public Date getDate() {
        return mDate;
    }
    public void setDate(Date date) {
        mDate = date;
    }
    public boolean isSolved() {
        return mSolved;
    }
    public void setSolved(boolean solved) {
        mSolved = solved;
    }
    @Override
    public String toString() {
        return mTitle;
    }
}

