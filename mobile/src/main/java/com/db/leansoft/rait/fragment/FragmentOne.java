package com.db.leansoft.rait.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.leansoft.R;
import com.db.leansoft.rait.fragment.chart.ChartsFragmentRait;

public class FragmentOne extends Fragment {

    TextView oneText;
    TextView twoText;
    TextView threeText;

    TextView oneMoney;
    TextView twoMoney;
    TextView threeMoney;

    private String one,two,three,fore,five,six;
      //ChartsFragmentRait chartsFragmentRait;

   public FragmentOne( String one, String two, String three, String fore, String five, String six){
       this.one = one;
       this.two = two;
       this.three = three;
       this.fore = fore;
       this.five = five;
       this.six = six;
       //this.chartsFragmentRait = chartsFragmentRait;

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rait_fragment, container, false);
        oneText = (TextView)v.findViewById(R.id.oneText);
        oneText.setText(one);
        twoText = (TextView)v.findViewById(R.id.twoText);
        twoText.setText(two);
        threeText = (TextView)v.findViewById(R.id.threeText);
        threeText.setText(three);
        oneMoney = (TextView)v.findViewById(R.id.oneMoney);
        oneMoney.setText(fore);
        twoMoney = (TextView)v.findViewById(R.id.twoMoney);
        twoMoney.setText(five);
        threeMoney = (TextView)v.findViewById(R.id.threeMoney);
        threeMoney.setText(six);


        return v;
    }
}