package com.db.leansoft.fragment;




import android.content.Intent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.db.leansoft.R;
import com.db.leansoft.core.CorFragmant;
import com.db.leansoft.core.VibrateService;
import com.db.leansoft.googleMap.MapsActivity;
import com.db.leansoft.head.HeadActivity;
import android.widget.Toast;

import com.db.leansoft.intent.SplashIntentActivity;
import com.db.leansoft.kpi.ItemListActivity;
import com.db.leansoft.potter.PotterActivity;
import com.db.leansoft.project.ProjectListActivity;
import com.db.leansoft.rait.RaitActivity;
import com.db.leansoft.repair.RepairActivity;
import com.db.leansoft.production.ProductionActivity;

public class MainFragment extends CorFragmant implements View.OnClickListener{
    ImageView kpi;
    ImageView head;
    ImageView intent;
    ImageView potter;
    ImageView production;
    ImageView project;
    ImageView repair;
    ImageView rait;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, parent, false);

        kpi =(ImageView) v.findViewById(R.id.kpi);
        kpi.setOnClickListener(this);

        rait = (ImageView)v.findViewById(R.id.rait);
        rait.setOnClickListener(this);

        production = (ImageView) v.findViewById(R.id.production);
        production.setOnClickListener(this);

        project = (ImageView)v.findViewById(R.id.project);
        project.setOnClickListener(this);

        head = (ImageView)v.findViewById(R.id.head);
        head.setOnClickListener(this);

        intent = (ImageView) v.findViewById(R.id.intent);
        intent.setOnClickListener(this);

        potter  = (ImageView)v.findViewById(R.id.potter);
        potter.setOnClickListener(this);

        repair = (ImageView)v.findViewById(R.id.repair);
        repair.setOnClickListener(this);


        return v;

    }

    public void onClick(View v){
        Intent intentVibrate = new Intent(getActivity().getApplicationContext(),VibrateService.class);
        getActivity().startService(intentVibrate);
        switch (v.getId()) {
            case R.id.kpi:
                startActivity(new Intent(getActivity(), ItemListActivity.class));
                getActivity().finish();

                break;
            case R.id.head:
                startActivity(new Intent(getActivity(), HeadActivity.class));
                getActivity().finish();
                break;
            case R.id.intent:
                startActivity(new Intent(getActivity(), SplashIntentActivity.class));
                getActivity().finish();
                break;
            case R.id.potter:
                startActivity(new Intent(getActivity(), PotterActivity.class));
                getActivity().finish();
                break;
            case R.id.production:
                startActivity(new Intent(getActivity(), ProductionActivity.class));
                getActivity().finish();
                break;
            case R.id.repair:
                startActivity(new Intent(getActivity(), RepairActivity.class));
                getActivity().finish();
                break;
            case R.id.rait:
                Toast toast2 = Toast.makeText(getActivity().getApplicationContext(),
                        "Модуль в разработке", Toast.LENGTH_SHORT);
                toast2.show();
                startActivity(new Intent(getActivity(), RaitActivity.class));
                getActivity().finish();
                break;
            case R.id.project:
                startActivity(new Intent(getActivity(), ProjectListActivity.class));
                getActivity().finish();
                break;
            default:
                break;
        }


    }
}
