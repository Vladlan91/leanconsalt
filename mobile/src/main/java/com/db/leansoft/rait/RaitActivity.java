package com.db.leansoft.rait;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;

import com.db.leansoft.rait.fragment.FragmentOne;


public class RaitActivity extends AppCompatActivity implements View.OnClickListener{

    ImageView reait_one;
    ImageView reait_two;
    ImageView reait_three;
    ImageView reait_fore;
    ImageView reait_five;
    ImageView reait_six;
    ImageView reait_seven;
    ImageView reait_eight;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rait);

        reait_one = (ImageView) findViewById(R.id.reait_one);
        reait_one.setOnClickListener(this);

        reait_two = (ImageView) findViewById(R.id.reait_two);
        reait_two.setOnClickListener(this);

        reait_three = (ImageView) findViewById(R.id.reait_three);
        reait_three.setOnClickListener(this);

        reait_fore = (ImageView) findViewById(R.id.reait_fore);
        reait_fore.setOnClickListener(this);

        reait_five = (ImageView) findViewById(R.id.reait_five);
        reait_five.setOnClickListener(this);

        reait_six = (ImageView) findViewById(R.id.reait_six);
        reait_six.setOnClickListener(this);

        reait_seven = (ImageView) findViewById(R.id.reait_seven);
        reait_seven.setOnClickListener(this);

        reait_eight = (ImageView) findViewById(R.id.reait_eight);
        reait_eight.setOnClickListener(this);

    }

    FragmentManager fm = getSupportFragmentManager();
    Fragment fragment = fm.findFragmentById(R.id.fragmentContainerTwo);


    public void onClick(View v){
        switch (v.getId()) {
            case R.id.reait_one:

                fragment = new FragmentOne("1233312","1233312","1233312","Плановые затраты на производство","Фактические затраты на производство","Потери");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_two:

                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_three:

                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_fore:

                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_five:

                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_six:

                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_seven:

                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            case R.id.reait_eight:
                fragment = new FragmentOne("1233312","1233312","1233312","1233312","1233312","1233312");
                fm.beginTransaction()
                        .add(R.id.fragmentContainerTwo, fragment)
                        .commit();
                break;
            default:
                break;
        }
        }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


    }

