package com.db.leansoft.rait.fragment.chart;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.production.barchart.BarCardOne;
import com.db.leansoft.production.linechart.LineCardOne;
import com.db.leansoft.production.linechart.LineCardThree;
import com.db.leansoft.production.linechart.LineCardTwo;


/**
 * Created by vlad on 29/04/2017.
 */

public class ChartsFragmentRait extends Fragment{



    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.char_fragmet_rait, container, false);



        (new LineCardOne((CardView) layout.findViewById(R.id.card1), getContext())).init();
        (new LineCardThree((CardView) layout.findViewById(R.id.card2), getContext())).init();
        (new BarCardOne((CardView) layout.findViewById(R.id.card3), getContext())).init();
        (new LineCardTwo((CardView) layout.findViewById(R.id.card9))).init();

        return layout;
    }


}


