package com.db.leansoft.task;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.db.leansoft.R;
import com.db.leansoft.core.CorFragmant;
import com.db.leansoft.core.VibrateService;

/**
 * Created by vlad on 25/02/2017.
 */

public class TaskFragment extends CorFragmant {

    TextView request;
    TextView staffTime;
    TextView resources;
    TextView kpiManagement;

    FragmentManager fm;
    Fragment fragment;
    Fragment taskFragmentOne;
    Fragment taskFragmentTwo;
    Fragment taskFragmentThree;
    Fragment taskFragmentFore;

    final private int COLOR_ONE = Color.parseColor("#227f76");
    final private int COLOR_TWO = Color.parseColor("#ffffff");



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.task_fragment, parent,false);
        request = (TextView)v.findViewById(R.id.request);
        request.setOnClickListener(this);

        staffTime = (TextView)v.findViewById(R.id.staffTime);
        staffTime.setOnClickListener(this);

        resources = (TextView)v.findViewById(R.id.resources);
        resources.setOnClickListener(this);

        kpiManagement = (TextView)v.findViewById(R.id.kpiManagement);
        kpiManagement.setOnClickListener(this);

        taskFragmentOne = new TaskFragmentOne();
        taskFragmentTwo = new CrimeListFragment();
        taskFragmentThree = new TaskFragmentThree();
        taskFragmentFore = new TaskFragmentFore();

        fm = getActivity().getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragmentContainer4);

        return v;
}

    private void fragmentShow(Fragment frag, TextView textView){
        fm = getActivity().getSupportFragmentManager();
        fragment = fm.findFragmentById(R.id.fragmentContainer4);
        fragment = frag;
        if (textView == request) {
            request.setTextColor(COLOR_ONE);
            request.setTextSize(24);

            staffTime.setTextSize(18);
            staffTime.setTextColor(COLOR_TWO);
            resources.setTextSize(18);
            resources.setTextColor(COLOR_TWO);
            kpiManagement.setTextSize(18);
            kpiManagement.setTextColor(COLOR_TWO);

        } else if (textView == staffTime) {
            staffTime.setTextColor(COLOR_ONE);
            staffTime.setTextSize(24);

            request.setTextColor(COLOR_TWO);
            request.setTextSize(18);
            resources.setTextSize(18);
            resources.setTextColor(COLOR_TWO);
            kpiManagement.setTextSize(18);
            kpiManagement.setTextColor(COLOR_TWO);
        } else if (textView == resources) {
            resources.setTextColor(COLOR_ONE);
            resources.setTextSize(24);

            request.setTextColor(COLOR_TWO);
            request.setTextSize(18);
            staffTime.setTextSize(18);
            staffTime.setTextColor(COLOR_TWO);
            kpiManagement.setTextSize(18);
            kpiManagement.setTextColor(COLOR_TWO);

        } else if (textView == kpiManagement) {
            kpiManagement.setTextSize(24);
            kpiManagement.setTextColor(COLOR_ONE);

            request.setTextColor(COLOR_TWO);
            request.setTextSize(18);
            staffTime.setTextSize(18);
            staffTime.setTextColor(COLOR_TWO);
            resources.setTextSize(18);
            resources.setTextColor(COLOR_TWO);
        }
        fm.beginTransaction()
                    .replace(R.id.fragmentContainer4, fragment)
                    .commit();
        }


    public void onClick(View v){
        Intent intentVibrate = new Intent(getActivity().getApplicationContext(),VibrateService.class);
        getActivity().startService(intentVibrate);
        switch (v.getId()) {
            case R.id.request:
                fragmentShow(taskFragmentOne,request);
                break;
            case R.id.staffTime:
                fragmentShow(taskFragmentTwo,staffTime);
                break;
            case R.id.resources:
                fragmentShow(taskFragmentThree,resources);
                break;
            case R.id.kpiManagement:
                fragmentShow(taskFragmentFore,kpiManagement);
            default:
                break;
        }
    }
}
