package com.db.leansoft;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.db.leansoft.aboutProdject.IntroActivity;
import com.db.leansoft.googleMap.MapsActivity;
import com.db.leansoft.login.LoginActivity;
import com.db.leansoft.statistik.DecoViewSampleActivity;
import com.db.leansoft.core.CorActivity;
import com.db.leansoft.core.NotificationService;
import com.db.leansoft.core.VibrateService;
import com.db.leansoft.fragment.MainFragment;
import com.db.leansoft.head.HeadActivity;
import com.db.leansoft.settings.SettingsActivity;
import com.db.leansoft.task.CrimeListActivity;


public class MainActivity extends CorActivity
        implements NavigationView.OnNavigationItemSelectedListener {
NotificationService notificationService;
    static {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intentnotification = new Intent(getApplicationContext(),NotificationService.class);
        startService(intentnotification);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {
            fragment = new MainFragment();
            fm.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "« У вас нет новых сообщений »", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intentVibrate = new Intent(getApplicationContext(),VibrateService.class);
        startService(intentVibrate);
        Intent intentnotification = new Intent(getApplicationContext(),NotificationService.class);
        startService(intentnotification);

        int id = item.getItemId();

        if (id == R.id.mainwindow) {
            startActivity(new Intent(this, HeadActivity.class));
            finish();
        } else if (id == R.id.statistics) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Модуль в разработке", Toast.LENGTH_SHORT);
            toast.show();
            startActivity(new Intent(this, DecoViewSampleActivity.class));
            finish();

        } else if (id == R.id.review) {
            startActivity(new Intent(this, IntroActivity.class));
            finish();

        } else if (id == R.id.nav_manage) {
            startActivity(new Intent(this, SettingsActivity.class));
            finish();


        } else if (id == R.id.message) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();

        } else if (id == R.id.task_managers) {
            startActivity(new Intent(this, CrimeListActivity.class));
            finish();
        }

        else if (id == R.id.contact) {
            startActivity(new Intent(this, MapsActivity.class));
            finish();

        } else if (id == R.id.news) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
