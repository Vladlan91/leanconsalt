package com.db.leansoft.kpi.chart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.kpi.chart.card.LineChartKPI;
import com.db.leansoft.kpi.dummy.MyChart;

/**
 * Created by vlad on 05/02/2017.
 */

public class KpiFragmentChar extends Fragment{



    MyChart myChart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.kpi_fragment_char,parent,false);
     myChart = new MyChart(new String[]{"2004", "2005", "2006", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016"},new float[][]{{98f, 97f, 96f, 96f, 95f, 99f, 95f, 98f, 96f, 96f, 97f, 96f},
               {98f, 97f, 96f, 96f, 95f, 99f, 95f, 98f, 96f, 96f, 97f, 96f}});

        (new LineChartKPI((CardView) layout.findViewById(R.id.card1), myChart, getContext())).init();
        return layout;
    }
}


