package com.db.leansoft.kpi.dummy;

/**
 * Created by vlad on 13/02/2017.
 */

public class MyChart {
    String[] mLabels;
    float[][] mValues;

    public MyChart() {
    }

    public MyChart(String[] mLabels, float[][] mValues) {
        this.mLabels = mLabels;
        this.mValues = mValues;
    }

    public float[][] getmValues() {
        return mValues;
    }

    public void setmValues(float[][] mValues) {
        this.mValues = mValues;
    }

    public String[] getmLabels() {
        return mLabels;
    }

    public void setmLabels(String[] mLabels) {
        this.mLabels = mLabels;
    }
}
