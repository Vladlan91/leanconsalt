package com.db.leansoft.potter.chart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.potter.chart.card.PotterLineCharOne;
import com.db.leansoft.potter.chart.card.PotterLineCharTwo;
/**
 * Created by vlad on 11/02/2017.
 */

public class PotterPotterFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.potter_potter_fragment,parent,false);

        (new PotterLineCharOne((CardView) layout.findViewById(R.id.card1), getContext())).init();
        (new PotterLineCharTwo((CardView) layout.findViewById(R.id.card2), getContext())).init();
        return layout;
    }
}
