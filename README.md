#LeanSoft 
![tablet][7]![leansreen.png](https://bitbucket.org/repo/KynEj8/images/1369645777-leansreen.png)

<https://vladlan91.github.io/>
Автоматизована систему управління бізнесом відноситься до програмного забезпечення, призначенням
якого є збільшення прибутку за рахунок внутрішніх резервів. Результат досягається за рахунок аналізу,
програмним забезпеченням, всього технологічного процесу із визначенням величини доданої вартості на
кожному етапі створення цінності продукту в розрізі години, доби, місяця, року. Порівнюючи дані
показники у відповідності процесу, до якого вони відносяться, програмне забезпечення, визначає
величину відхилень, відповідно втрати, які компанія несе із-за операційних порушень. Так програмне
забезпечення аналізує кращі / гірші питомі величини для відображення потенціалу компанії, що дає змогу
визначити і створити стандарт операційної процедури. До відхилень відносяться цільові показники, які
ставила перед собою компанія на етапі формування бізнес плану на наступний рік, до них відносяться
показники які відображають ефективність роботи.

Область застосування:

* ``Хімічна промисловість;``
* ``Чорна та кольорова металургія;``
* ``Машинобудування;``
* ``Приладобудування;``
* ``Легка промисловість;``
* ``Аграрна промисловість;``
* ``Харчова промисловість;``
* ``Консалтингові компанії;``

<https://www.youtube.com/embed/IbHwIBCK2ms>

Основні налаштування
--------------------

MinSdkVersion 22 ![tablet][8]

###Gradle library
``` groovy
    compile 'com.android.support:appcompat-v7:24.2.1'
    compile 'com.android.support:design:24.2.1'
    compile 'com.android.support:cardview-v7:24.2.1'
    compile 'com.larswerkman:HoloColorPicker:1.5'
    compile 'com.android.support:support-v4:24.2.1'
    compile 'com.google.android.gms:play-services:10.0.1'
    compile 'com.android.support.constraint:constraint-layout:1.0.0-alpha7'
    compile 'com.android.support:recyclerview-v7:24.2.1'
    compile 'com.android.support:support-vector-drawable:24.2.1'
```

[1]: ./art/2.3.0-1.gif
[2]: ./art/2.3.0-2.gif
[7]: ./art/phone.png
[8]: ./art/watch.png