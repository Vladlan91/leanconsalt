package com.db.leansoft.task;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;
import com.db.leansoft.core.SingleFragmentActivity;

/**
 * Created by User on 13.02.2017.
 */
public class CrimeListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new TaskFragmentNull();

    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
