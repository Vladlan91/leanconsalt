package com.db.leansoft.intent;



import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


    public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        private String[] mTabTitles;

        public MyFragmentPagerAdapter(FragmentManager fm, String[] mTabTitles) {
            super(fm);
            this.mTabTitles = mTabTitles;
        }


        @Override
        public android.support.v4.app.Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new FragmentA();
                case 1:
                    return new FragmentB();
                case 2:
                    return new FragmentС();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return this.mTabTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.mTabTitles[position];
        }
    }

