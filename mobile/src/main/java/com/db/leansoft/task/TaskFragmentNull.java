package com.db.leansoft.task;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.core.CorFragmant;

/**
 * Created by vlad on 05/03/2017.
 */

public class TaskFragmentNull extends CorFragmant{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.task_fragment_null, parent,false);

        return v;
    }
}
