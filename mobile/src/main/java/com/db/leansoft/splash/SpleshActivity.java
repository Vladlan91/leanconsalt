package com.db.leansoft.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;

public class SpleshActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private int progressStatus = 0;
    private TextView tv;
    private TextView iv;
    private Handler handler = new Handler();
    Thread myThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        tv = (TextView) findViewById(R.id.start);
        iv = (TextView) findViewById(R.id.textstart);
        android.view.animation.Animation myanim = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        android.view.animation.Animation myanim2 = AnimationUtils.loadAnimation(this,R.anim.transle);
        tv.startAnimation(myanim);
        iv.startAnimation(myanim2);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                        }
                    });
                    try {
                        Thread.sleep(60);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(6000);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
    @Override
    public void onBackPressed() {
        myThread.interrupt();
        finish();
    }
}
