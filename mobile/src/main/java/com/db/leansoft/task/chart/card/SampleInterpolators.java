package com.db.leansoft.task.chart.card;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;

import com.db.leansoft.R;
import com.db.leansoft.statistik.SampleFragment;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

/**
 * Created by vlad on 05/03/2017.
 */

public class SampleInterpolators extends SampleFragment{
    private int mSeries1Index;

    public SampleInterpolators() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sample_interpolators, container, false);
    }

    @Override
    protected void createTracks() {
        if (getView() != null) {
            createTracks(R.id.dynamicArcView1, new LinearInterpolator(), Color.parseColor("#e21b09"));
            createTracks(R.id.dynamicArcView3, new AccelerateInterpolator(), Color.parseColor("#f2c309"));
            createTracks(R.id.dynamicArcView5, new BounceInterpolator(), Color.parseColor("#71e208"));
        }
    }

    @Override
    protected void setupEvents() {
        if (getView() != null) {
            myDynamicArcView1(R.id.dynamicArcView1);
            myDynamicArcView3(R.id.dynamicArcView3);
            myDynamicArcView5(R.id.dynamicArcView5);
        }
    }

    private void createTracks(int arcViewId, Interpolator interpolator, int color) {
        final View view = getView();
        if (view == null) {
            return;
        }

        final DecoView decoView = (DecoView) view.findViewById(arcViewId);
        if (decoView == null) {
            return;
        }

        decoView.deleteAll();
        decoView.configureAngles(220, 0);

        final float mSeriesMax = 50f;
        SeriesItem arcBackTrack = new SeriesItem.Builder(Color.argb(255, 228, 228, 228))
                .setRange(0, mSeriesMax, mSeriesMax)
                .setLineWidth(getDimension(12f))
                .build();

        decoView.addSeries(arcBackTrack);

        SeriesItem seriesItem = new SeriesItem.Builder(color)
                .setRange(0, mSeriesMax, 0)
                .setInterpolator(interpolator)
                .setLineWidth(getDimension(12f))
                .setSpinDuration(5000)
                .setSpinClockwise(false)
                .build();

        mSeries1Index = decoView.addSeries(seriesItem);

        SeriesItem seriesItem1 = new SeriesItem.Builder(color)
                .setRange(0, mSeriesMax, 0)
                .setInterpolator(interpolator)
                .setLineWidth(getDimension(12f))
                .setSpinDuration(5000)
                .setSpinClockwise(false)
                .build();

        mSeries1Index = decoView.addSeries(seriesItem1);

    }

    /**
     * Create a series of events that build a demonstration of moving and positioning
     * the arcs in the DecoView
     */
    private void myDynamicArcView1(final int arcId) {
        final View view = getView();
        if (view == null) {
            return;
        }
        final DecoView decoView = (DecoView) view.findViewById(arcId);
        if (decoView == null || decoView.isEmpty()) {
            throw new IllegalStateException("Unable to add events to empty View");
        }

        decoView.executeReset();

        decoView.addEvent(new DecoEvent.Builder(50).setIndex(mSeries1Index).setDelay(1000).build());
        decoView.addEvent(new DecoEvent.Builder(0).setIndex(mSeries1Index).setDelay(22000)
                .setListener(new DecoEvent.ExecuteEventListener() {
                    @Override
                    public void onEventStart(DecoEvent event) {

                    }

                    @Override
                    public void onEventEnd(DecoEvent event) {
                        setupEvents();
                    }
                })
                .build());
    }
    private void myDynamicArcView3(final int arcId) {
        final View view = getView();
        if (view == null) {
            return;
        }
        final DecoView decoView = (DecoView) view.findViewById(arcId);
        if (decoView == null || decoView.isEmpty()) {
            throw new IllegalStateException("Unable to add events to empty View");
        }

        decoView.executeReset();

        decoView.addEvent(new DecoEvent.Builder(40).setIndex(mSeries1Index).setDelay(1000).build());
        decoView.addEvent(new DecoEvent.Builder(0).setIndex(mSeries1Index).setDelay(22000)
                .setListener(new DecoEvent.ExecuteEventListener() {
                    @Override
                    public void onEventStart(DecoEvent event) {

                    }

                    @Override
                    public void onEventEnd(DecoEvent event) {

                    }
                })
                .build());
    }
    private void myDynamicArcView5(final int arcId) {
        final View view = getView();
        if (view == null) {
            return;
        }
        final DecoView decoView = (DecoView) view.findViewById(arcId);
        if (decoView == null || decoView.isEmpty()) {
            throw new IllegalStateException("Unable to add events to empty View");
        }

        decoView.executeReset();

        decoView.addEvent(new DecoEvent.Builder(30).setIndex(mSeries1Index).setDelay(1000).build());
        decoView.addEvent(new DecoEvent.Builder(0).setIndex(mSeries1Index).setDelay(22000)
                .setListener(new DecoEvent.ExecuteEventListener() {
                    @Override
                    public void onEventStart(DecoEvent event) {

                    }

                    @Override
                    public void onEventEnd(DecoEvent event) {

                    }
                })
                .build());
    }
}


