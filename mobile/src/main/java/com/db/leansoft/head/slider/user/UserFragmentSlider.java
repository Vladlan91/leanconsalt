package com.db.leansoft.head.slider.user;

import android.content.Context;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.LinearLayout;

import android.widget.TextView;
import com.db.leansoft.R;
import com.db.leansoft.kpi.ItemListActivity;


public class UserFragmentSlider extends Fragment implements View.OnClickListener{
    private ViewPager viewPager;
    private UserFragmentSlider.ViewPagerAdapter viewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnMore;
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.head_user_slider, parent, false);

        viewPager = (ViewPager) v.findViewById(R.id.view_pager_head_user);
        dotsLayout = (LinearLayout) v.findViewById(R.id.layoutDots);
        btnMore = (Button) v.findViewById(R.id.btn_more);
        btnMore.setOnClickListener(this);
        layouts = new int[]{
                R.layout.head_user_slider2,
                R.layout.head_user_slider1};

        addBottomDots(0);

        viewPagerAdapter = new UserFragmentSlider.ViewPagerAdapter();
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


    return v;

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_more:
                startActivity(new Intent(getActivity(), ItemListActivity.class));
                getActivity().finish();
                break;

        }
    }

    public  void btnNextClick(View v)
    {
        int current = getItem(1);
        if (current < layouts.length) {
            viewPager.setCurrentItem(current);
        } else {

        }
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);


            if (position == layouts.length - 1) {
                btnMore.setText(getString(R.string.more));
            } else {
                btnMore.setText(getString(R.string.next));
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this.getContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_inactive));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_active));
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    public class ViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;


        public ViewPagerAdapter() {

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


}
