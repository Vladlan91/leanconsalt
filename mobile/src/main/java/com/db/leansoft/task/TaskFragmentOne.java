package com.db.leansoft.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.leansoft.R;
import com.db.leansoft.core.CorFragmant;

/**
 * Created by vlad on 28/02/2017.
 */

public class TaskFragmentOne extends CorFragmant{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.task_fragment_one, parent,false);

        return v;
    }

}
