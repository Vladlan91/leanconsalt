
package com.db.leansoft.production.controller;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.leansoft.R;
import com.db.leansoft.production.barchart.BarCardOne;
import com.db.leansoft.production.barchart.BarCardThree;
import com.db.leansoft.production.barchart.BarCardTwo;
import com.db.leansoft.production.linechart.LineCardOne;
import com.db.leansoft.production.linechart.LineCardThree;
import com.db.leansoft.production.linechart.LineCardTwo;
import com.db.leansoft.production.stackedchart.StackedCardOne;
import com.db.leansoft.production.stackedchart.StackedCardThree;
import com.db.leansoft.production.stackedchart.StackedCardTwo;


public class ChartsFragment extends Fragment {

	static {
		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			  Bundle savedInstanceState) {

		View layout = inflater.inflate(R.layout.charts, container, false);

		((AppCompatActivity) getActivity()).setSupportActionBar(
				  (Toolbar) layout.findViewById(R.id.toolbar));
		((TextView) layout.findViewById(R.id.title)).setTypeface(
				  Typeface.createFromAsset(getContext().getAssets(), "Ponsi-Regular.otf"));

		(new LineCardOne((CardView) layout.findViewById(R.id.card1), getContext())).init();
		(new LineCardThree((CardView) layout.findViewById(R.id.card2), getContext())).init();
		(new BarCardOne((CardView) layout.findViewById(R.id.card3), getContext())).init();
		(new StackedCardThree((CardView) layout.findViewById(R.id.card4), getContext())).init();
		(new StackedCardOne((CardView) layout.findViewById(R.id.card5))).init();
		(new BarCardThree((CardView) layout.findViewById(R.id.card6), getContext())).init();
		(new BarCardTwo((CardView) layout.findViewById(R.id.card7), getContext())).init();
		(new StackedCardTwo((CardView) layout.findViewById(R.id.card8))).init();
		(new LineCardTwo((CardView) layout.findViewById(R.id.card9))).init();

		return layout;
	}


}
