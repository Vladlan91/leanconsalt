package com.db.leansoft.repair.chart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.kpi.chart.card.BarCardTwoP;
import com.db.leansoft.repair.chart.card.BarCardTwoRepiar;

/**
 * Created by vlad on 08/02/2017.
 */

public class RepiarFragmentChar extends Fragment{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.rpair_fragment_char,parent,false);

        (new BarCardTwoRepiar((CardView) layout.findViewById(R.id.card7), getContext())).init();
        return layout;
    }
}

