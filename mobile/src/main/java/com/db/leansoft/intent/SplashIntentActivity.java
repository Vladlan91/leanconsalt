package com.db.leansoft.intent;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.db.leansoft.MainActivity;
import com.db.leansoft.R;
import com.db.leansoft.core.CorActivity;

/**
 * Created by vlad on 28/02/2017.
 */

public class SplashIntentActivity extends CorActivity{

    private ImageView intent_part_one;
    private TextView textKPI;
    private ImageView intent_part_two;
    Thread myThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_intant);
        intent_part_one = (ImageView) findViewById(R.id.intent_part_one);
        intent_part_two = (ImageView) findViewById(R.id.intent_part_two);

        android.view.animation.Animation myanim = AnimationUtils.loadAnimation(this,R.anim.mytransition);
        android.view.animation.Animation myanim2 = AnimationUtils.loadAnimation(this,R.anim.transle);

        intent_part_one.startAnimation(myanim);
        intent_part_two.startAnimation(myanim2);


         myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(7000);
                    Intent intent = new Intent(getApplicationContext(), IntentActivity.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
    @Override
    public void onBackPressed() {
        myThread.interrupt();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}


