package com.db.leansoft.project.chart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.leansoft.R;
import com.db.leansoft.core.CorFragmant;
import com.db.leansoft.kpi.chart.card.BarCardTwoP;
import com.db.leansoft.project.chart.card.BarCardProject;

/**
 * Created by vlad on 03/03/2017.
 */

public class BarCardFragment extends Fragment{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.project_fragment_char,parent,false);

        (new BarCardProject((CardView) layout.findViewById(R.id.card3), getContext())).init();
        return layout;
    }
}

