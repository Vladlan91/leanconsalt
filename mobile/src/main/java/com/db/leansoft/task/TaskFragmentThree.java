package com.db.leansoft.task;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.db.leansoft.R;
import com.db.leansoft.core.CorFragmant;

/**
 * Created by vlad on 04/03/2017.
 */

public class TaskFragmentThree extends CorFragmant{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.task_fragment_three, parent,false);

        return v;
    }
}
